int AC_step_number
real AC_dt

/*
real AC_cs_sound, AC_cp_sound
real AC_gamma, AC_nu_visc, AC_zeta, AC_eta
real AC_lnrho0, AC_mu0, AC_lnT0
*/
// Note: not used by mhdsolver.ac at the moment (src/utils/modelsolver.c does)
hostdefine AC_INTEGRATION_ENABLED // Enables acIntegrate() and other non-generic library functions
hostdefine LDENSITY (1)
hostdefine LHYDRO (1)
hostdefine LMAGNETIC (1)
hostdefine LENTROPY (1)
hostdefine LTEMPERATURE (0)
hostdefine LFORCING (0)
hostdefine LUPWD (1)
hostdefine LSINK (0)
hostdefine LBFIELD (1 && LMAGNETIC) // bfield only relevant if magnetic is on
hostdefine LSHOCK (0)
//hostdefine AC_THERMAL_CONDUCTIVITY (0.001) // Now a config parameter, AC_K_heatcond, as it should be. 
hostdefine R_PI (AC_REAL_PI)

// Enables a set of special reductions. Currently for primarily for testing
// purposes:  
hostdefine LSPECIAL_REDUCTIONS (0)  

Field VTXBUF_LNRHO, VTXBUF_UUX, VTXBUF_UUY, VTXBUF_UUZ

#if LMAGNETIC
Field VTXBUF_AX, VTXBUF_AY, VTXBUF_AZ
#endif

#if LENTROPY 
Field VTXBUF_ENTROPY
#endif

#if LBFIELD
Field BFIELDX, BFIELDY, BFIELDZ
#endif

#if LSHOCK 
Field VTXBUF_SHOCK
#endif

// Additional params needed by standalone & standalone_mpi
// diagnostics period
int AC_save_steps

// snapshot period
int AC_bin_steps
real AC_bin_save_t

// slices output period
int AC_slice_steps
real AC_slice_save_t

// maximun number of time snapshots during runtime 
// Set AC_num_snapshots < 0 for unlimited snapshots
int AC_num_snapshots

// max simulation time
int AC_max_steps
real AC_max_time

// Forcing parameter generation period (if forcing is on)
int AC_forcing_period_steps
real AC_forcing_period_t

// Initial time step index, default should be 0
int AC_start_step

real AC_dsx, AC_dsy, AC_dsz // WARNING UNUSED! NEED TO BE COMPILE-TIME CONSTANTS

hostdefine DSX (0.012271846303085129837744700715935558141395192)
hostdefine DSY (0.012271846303085129837744700715935558141395192)
hostdefine DSZ (0.012271846303085129837744700715935558141395192)
#define AC_inv_dsx (1. / DSX)
#define AC_inv_dsy (1. / DSY)
#define AC_inv_dsz (1. / DSZ)

// Real params
// Spacing
real AC_dsmin
// physical grid
real AC_xlen
real AC_ylen
real AC_zlen
real AC_xorig
real AC_yorig
real AC_zorig
// Physical units
real AC_unit_density
real AC_unit_velocity
real AC_unit_length
real AC_unit_magnetic
// properties of gravitating star
real AC_star_pos_x
real AC_star_pos_y
real AC_star_pos_z
real AC_M_star
// properties of sink particle
real AC_sink_pos_x
real AC_sink_pos_y
real AC_sink_pos_z
real AC_M_sink
real AC_M_sink_init
real AC_M_sink_Msun
real AC_soft
real AC_accretion_range
real AC_switch_accretion
//  Run params
real AC_cdt
real AC_cdtv
real AC_cdts
real AC_nu_visc
real AC_cs_sound
real AC_eta
real AC_mu0
real AC_cp_sound
real AC_gamma
real AC_cv_sound
real AC_lnT0
real AC_lnrho0
real AC_zeta
real AC_trans
real AC_nu_shock
real AC_K_heatcond

//  Parameters for generic boundary conditions, this is a dummy variable for testing, please use different ones for each field
real AC_boundary_derivative

//  Parameters only used in boundary conditions at the moment
//  If you feel they should be elsewhere, please move them
real AC_hcond0_kramers
real AC_hflux
real AC_n_kramers
real AC_sigma_SBt
real AC_chi
real AC_chi_t
real AC_chi_t_prof1

//  Initial condition params
real AC_ampl_lnrho
real AC_ampl_uu
real AC_init_ampl_uu
real AC_angl_uu
real AC_lnrho_edge
real AC_lnrho_out
real AC_ampl_aa
real AC_init_k_wave
real AC_init_sigma_hel
real AC_init_shell_radius
real AC_init_shell_width
//  Forcing parameters. User configured.
real AC_forcing_magnitude
real AC_relhel
real AC_kmin
real AC_kmax
real AC_switch_forcing
//  Forcing parameters. Set by the generator.
real AC_forcing_phase
real AC_k_forcex
real AC_k_forcey
real AC_k_forcez
real AC_kaver
real AC_ff_hel_rex
real AC_ff_hel_rey
real AC_ff_hel_rez
real AC_ff_hel_imx
real AC_ff_hel_imy
real AC_ff_hel_imz
//  Additional helper params  //  (deduced from other params do not set these directly!)
real AC_G_const
real AC_GM_star
real AC_unit_mass
real AC_sq2GM_star
real AC_cs2_sound
//
real AC_current_time
// For special reductions
real AC_center_x
real AC_center_y
real AC_center_z
real AC_sum_radius
real AC_window_radius

#define UU Field3(VTXBUF_UUX, VTXBUF_UUY, VTXBUF_UUZ)

#if LMAGNETIC
#define AA Field3(VTXBUF_AX, VTXBUF_AY, VTXBUF_AZ)
#endif

#define DER1_3 (1. / 60.)
#define DER1_2 (-3. / 20.)
#define DER1_1 (3. / 4.)
#define DER1_0 (0)

#define DER2_3 (1. / 90.)
#define DER2_2 (-3. / 20.)
#define DER2_1 (3. / 2.)
#define DER2_0 (-49. / 18.)

#define DERX_3 (2. / 720.)
#define DERX_2 (-27. / 720.)
#define DERX_1 (270. / 720.)
#define DERX_0 (0)

#define DER6UPWD_3 (  1. / 60.)  
#define DER6UPWD_2 ( -6. / 60.)   
#define DER6UPWD_1 ( 15. / 60.)     
#define DER6UPWD_0 (-20. / 60.)  

#if LSHOCK

#include "smooth_kernel.ach" 

#endif 

Stencil value {
    [0][0][0] = 1
}

/*
// A hax to access close by stencil values
Stencil value_xm3 {
    [0][0][-3] = 1
}
Stencil value_xm2 {
    [0][0][-2] = 1
}
Stencil value_xm1 {
    [0][0][-1] = 1
}
Stencil value_xp1 {
    [0][0][1] = 1
}
Stencil value_xp2 {
    [0][0][2] = 1
}
Stencil value_xp3 {
    [0][0][3] = 1
}

Stencil value_ym3 {
    [0][-3][0] = 1
}
Stencil value_ym2 {
    [0][-2][0] = 1
}
Stencil value_ym1 {
    [0][-1][0] = 1
}
Stencil value_yp1 {
    [0][1][0] = 1
}
Stencil value_yp2 {
    [0][2][0] = 1
}
Stencil value_yp3 {
    [0][3][0] = 1
}

Stencil value_zm3 {
    [-3][0][0] = 1
}
Stencil value_zm2 {
    [-2][0][0] = 1
}
Stencil value_zm1 {
    [-1][0][0] = 1
}
Stencil value_zp1 {
    [1][0][0] = 1
}
Stencil value_zp2 {
    [2][0][0] = 1
}
Stencil value_zp3 {
    [3][0][0] = 1
}
*/

//MV: Based on the standard rules of multiplications, why -AC_inv_dsx etc could
//MV: not be outside of the stencil operation? 

Stencil derx {
    [0][0][-3] = -AC_inv_dsx * DER1_3,
    [0][0][-2] = -AC_inv_dsx * DER1_2,
    [0][0][-1] = -AC_inv_dsx * DER1_1,
    [0][0][1]  = AC_inv_dsx * DER1_1,
    [0][0][2]  = AC_inv_dsx * DER1_2,
    [0][0][3]  = AC_inv_dsx * DER1_3
}

Stencil dery {
    [0][-3][0] = -AC_inv_dsy * DER1_3,
    [0][-2][0] = -AC_inv_dsy * DER1_2,
    [0][-1][0] = -AC_inv_dsy * DER1_1,
    [0][1][0]  = AC_inv_dsy * DER1_1,
    [0][2][0]  = AC_inv_dsy * DER1_2,
    [0][3][0]  = AC_inv_dsy * DER1_3
}

Stencil derz {
    [-3][0][0] = -AC_inv_dsz * DER1_3,
    [-2][0][0] = -AC_inv_dsz * DER1_2,
    [-1][0][0] = -AC_inv_dsz * DER1_1,
    [1][0][0]  = AC_inv_dsz * DER1_1,
    [2][0][0]  = AC_inv_dsz * DER1_2,
    [3][0][0]  = AC_inv_dsz * DER1_3
}

Stencil derxx {
    [0][0][-3] = AC_inv_dsx * AC_inv_dsx * DER2_3,
    [0][0][-2] = AC_inv_dsx * AC_inv_dsx * DER2_2,
    [0][0][-1] = AC_inv_dsx * AC_inv_dsx * DER2_1,
    [0][0][0]  = AC_inv_dsx * AC_inv_dsx * DER2_0,
    [0][0][1]  = AC_inv_dsx * AC_inv_dsx * DER2_1,
    [0][0][2]  = AC_inv_dsx * AC_inv_dsx * DER2_2,
    [0][0][3]  = AC_inv_dsx * AC_inv_dsx * DER2_3
}

Stencil deryy {
    [0][-3][0] = AC_inv_dsy * AC_inv_dsy * DER2_3,
    [0][-2][0] = AC_inv_dsy * AC_inv_dsy * DER2_2,
    [0][-1][0] = AC_inv_dsy * AC_inv_dsy * DER2_1,
    [0][0][0]  = AC_inv_dsy * AC_inv_dsy * DER2_0,
    [0][1][0]  = AC_inv_dsy * AC_inv_dsy * DER2_1,
    [0][2][0]  = AC_inv_dsy * AC_inv_dsy * DER2_2,
    [0][3][0]  = AC_inv_dsy * AC_inv_dsy * DER2_3
}

Stencil derzz {
    [-3][0][0] = AC_inv_dsz * AC_inv_dsz * DER2_3,
    [-2][0][0] = AC_inv_dsz * AC_inv_dsz * DER2_2,
    [-1][0][0] = AC_inv_dsz * AC_inv_dsz * DER2_1,
    [0][0][0]  = AC_inv_dsz * AC_inv_dsz * DER2_0,
    [1][0][0]  = AC_inv_dsz * AC_inv_dsz * DER2_1,
    [2][0][0]  = AC_inv_dsz * AC_inv_dsz * DER2_2,
    [3][0][0]  = AC_inv_dsz * AC_inv_dsz * DER2_3
}

Stencil derxy {
    [0][-3][-3] = AC_inv_dsx * AC_inv_dsy * DERX_3,
    [0][-2][-2] = AC_inv_dsx * AC_inv_dsy * DERX_2,
    [0][-1][-1] = AC_inv_dsx * AC_inv_dsy * DERX_1,
    [0][0][0]  = AC_inv_dsx * AC_inv_dsy * DERX_0,
    [0][1][1]  = AC_inv_dsx * AC_inv_dsy * DERX_1,
    [0][2][2]  = AC_inv_dsx * AC_inv_dsy * DERX_2,
    [0][3][3]  = AC_inv_dsx * AC_inv_dsy * DERX_3,
    [0][-3][3] = -AC_inv_dsx * AC_inv_dsy * DERX_3,
    [0][-2][2] = -AC_inv_dsx * AC_inv_dsy * DERX_2,
    [0][-1][1] = -AC_inv_dsx * AC_inv_dsy * DERX_1,
    [0][1][-1] = -AC_inv_dsx * AC_inv_dsy * DERX_1,
    [0][2][-2] = -AC_inv_dsx * AC_inv_dsy * DERX_2,
    [0][3][-3] = -AC_inv_dsx * AC_inv_dsy * DERX_3
}

Stencil derxz {
    [-3][0][-3] = AC_inv_dsx * AC_inv_dsz * DERX_3,
    [-2][0][-2] = AC_inv_dsx * AC_inv_dsz * DERX_2,
    [-1][0][-1] = AC_inv_dsx * AC_inv_dsz * DERX_1,
    [0][0][0]  = AC_inv_dsx * AC_inv_dsz * DERX_0,
    [1][0][1]  = AC_inv_dsx * AC_inv_dsz * DERX_1,
    [2][0][2]  = AC_inv_dsx * AC_inv_dsz * DERX_2,
    [3][0][3]  = AC_inv_dsx * AC_inv_dsz * DERX_3,
    [-3][0][3] = -AC_inv_dsx * AC_inv_dsz * DERX_3,
    [-2][0][2] = -AC_inv_dsx * AC_inv_dsz * DERX_2,
    [-1][0][1] = -AC_inv_dsx * AC_inv_dsz * DERX_1,
    [1][0][-1] = -AC_inv_dsx * AC_inv_dsz * DERX_1,
    [2][0][-2] = -AC_inv_dsx * AC_inv_dsz * DERX_2,
    [3][0][-3] = -AC_inv_dsx * AC_inv_dsz * DERX_3
}

Stencil deryz {
    [-3][-3][0] = AC_inv_dsy * AC_inv_dsz * DERX_3,
    [-2][-2][0] = AC_inv_dsy * AC_inv_dsz * DERX_2,
    [-1][-1][0] = AC_inv_dsy * AC_inv_dsz * DERX_1,
    [0][0][0]  = AC_inv_dsy * AC_inv_dsz * DERX_0,
    [1][1][0]  = AC_inv_dsy * AC_inv_dsz * DERX_1,
    [2][2][0]  = AC_inv_dsy * AC_inv_dsz * DERX_2,
    [3][3][0]  = AC_inv_dsy * AC_inv_dsz * DERX_3,
    [-3][3][0] = -AC_inv_dsy * AC_inv_dsz * DERX_3,
    [-2][2][0] = -AC_inv_dsy * AC_inv_dsz * DERX_2,
    [-1][1][0] = -AC_inv_dsy * AC_inv_dsz * DERX_1,
    [1][-1][0] = -AC_inv_dsy * AC_inv_dsz * DERX_1,
    [2][-2][0] = -AC_inv_dsy * AC_inv_dsz * DERX_2,
    [3][-3][0] = -AC_inv_dsy * AC_inv_dsz * DERX_3
}

Stencil der6x_upwd {
    [0][0][-3] =  AC_inv_dsx * DER6UPWD_3,
    [0][0][-2] =  AC_inv_dsx * DER6UPWD_2,
    [0][0][-1] =  AC_inv_dsx * DER6UPWD_1,
    [0][0][0]  =  AC_inv_dsx * DER6UPWD_0,
    [0][0][1]  =  AC_inv_dsx * DER6UPWD_1,
    [0][0][2]  =  AC_inv_dsx * DER6UPWD_2,
    [0][0][3]  =  AC_inv_dsx * DER6UPWD_3
}

Stencil der6y_upwd {
    [0][-3][0] =  AC_inv_dsy * DER6UPWD_3,
    [0][-2][0] =  AC_inv_dsy * DER6UPWD_2,
    [0][-1][0] =  AC_inv_dsy * DER6UPWD_1,
    [0][0][0]  =  AC_inv_dsy * DER6UPWD_0,
    [0][1][0]  =  AC_inv_dsy * DER6UPWD_1,
    [0][2][0]  =  AC_inv_dsy * DER6UPWD_2,
    [0][3][0]  =  AC_inv_dsy * DER6UPWD_3
}

Stencil der6z_upwd {
    [-3][0][0] =  AC_inv_dsz * DER6UPWD_3,
    [-2][0][0] =  AC_inv_dsz * DER6UPWD_2,
    [-1][0][0] =  AC_inv_dsz * DER6UPWD_1,
    [0][0][0]  =  AC_inv_dsz * DER6UPWD_0,
    [1][0][0]  =  AC_inv_dsz * DER6UPWD_1,
    [2][0][0]  =  AC_inv_dsz * DER6UPWD_2,
    [3][0][0]  =  AC_inv_dsz * DER6UPWD_3
}


vecvalue(v) {
    return real3(value(v.x), value(v.y), value(v.z))
}

vecvalue_abs(v) {
    return real3(fabs(value(v.x)), fabs(value(v.y)), fabs(value(v.z)))
}

gradient(s) {
    return real3(derx(s), dery(s), derz(s))
}

gradient6_upwd(s) {
    return real3(der6x_upwd(s), der6y_upwd(s), der6z_upwd(s))
}

gradients_upwd(v) {
    return Matrix(gradient6_upwd(v.x), gradient6_upwd(v.y), gradient6_upwd(v.z))
}

gradients(v) {
    return Matrix(gradient(v.x), gradient(v.y), gradient(v.z))
}

divergence(v) {
    return derx(v.x) + dery(v.y) + derz(v.z)
}

curl(v) {
    return real3(dery(v.z) - derz(v.y), derz(v.x) - derx(v.z), derx(v.y) - dery(v.x))
}

laplace(s) {
    return derxx(s) + deryy(s) + derzz(s)
}

veclaplace(v) {
    return real3(laplace(v.x), laplace(v.y), laplace(v.z))
}

#if LMAGNETIC
induction() {
    return cross(vecvalue(UU), curl(AA)) + AC_eta * veclaplace(AA)
}
#endif

stress_tensor(v) {
    Matrix S

    S.data[0][0] = (2.0 / 3.0) * derx(v.x) - (1.0 / 3.0) * (dery(v.y) + derz(v.z))
    S.data[0][1] = (1.0 / 2.0) * (dery(v.x) + derx(v.y))
    S.data[0][2] = (1.0 / 2.0) * (derz(v.x) + derx(v.z))

    S.data[1][0] = S.data[0][1]
    S.data[1][1] = (2.0 / 3.0) * dery(v.y) - (1.0 / 3.0) * (derx(v.x) + derz(v.z))
    S.data[1][2] = (1.0 / 2.0) * (derz(v.y) + dery(v.z))

    S.data[2][0] = S.data[0][2]
    S.data[2][1] = S.data[1][2]
    S.data[2][2] = (2.0 / 3.0) * derz(v.z) - (1.0 / 3.0) * (derx(v.x) + dery(v.y))

    return S
}

gradient_of_divergence(v) {
    return real3(
        derxx(v.x) + derxy(v.y) + derxz(v.z),
        derxy(v.x) + deryy(v.y) + deryz(v.z),
        derxz(v.x) + deryz(v.y) + derzz(v.z)
    )
}

contract(mat) {
    return dot(mat.row(0), mat.row(0)) +
           dot(mat.row(1), mat.row(1)) +
           dot(mat.row(2), mat.row(2))
}

length(v1,v2) {
    return sqrt( dot(v1-v2,v1-v2) )
}

grid_position() {
    return real3((globalVertexIdx.x - AC_nx_min) * AC_dsx, 
                 (globalVertexIdx.y - AC_ny_min) * AC_dsy, 
                 (globalVertexIdx.z - AC_nz_min) * AC_dsz)
}

grid_centre() {
    return real3(((globalGridN.x-1) * AC_dsx)/2.0, 
                 ((globalGridN.y-1) * AC_dsy)/2.0, 
                 ((globalGridN.z-1) * AC_dsz)/2.0)
}


#if LFORCING
// The Pencil Code forcing_hel_noshear(), manual Eq. 222, inspired forcing function with adjustable
// helicity
helical_forcing(k_force, xx, ff_re, ff_im, phi)
{
    real3 yy
    yy.x = xx.x * (2.0 * R_PI / (AC_dsx * globalGridN.x))
    yy.y = xx.y * (2.0 * R_PI / (AC_dsy * globalGridN.x))
    yy.z = xx.z * (2.0 * R_PI / (AC_dsz * globalGridN.x))

    cos_phi     = cos(phi)
    sin_phi     = sin(phi)
    cos_k_dot_x = cos(dot(k_force, yy))
    sin_k_dot_x = sin(dot(k_force, yy))
    real_comp_phase = cos_k_dot_x * cos_phi - sin_k_dot_x * sin_phi
    imag_comp_phase = cos_k_dot_x * sin_phi + sin_k_dot_x * cos_phi

    force = real3(ff_re.x * real_comp_phase - ff_im.x * imag_comp_phase,
                  ff_re.y * real_comp_phase - ff_im.y * imag_comp_phase,
                  ff_re.z * real_comp_phase - ff_im.z * imag_comp_phase)

    return force
}

forcing()
{
    xx = real3((globalVertexIdx.x - AC_nx_min) * AC_dsx,
               (globalVertexIdx.y - AC_ny_min) * AC_dsy,
               (globalVertexIdx.z - AC_nz_min) * AC_dsz)
    cs  = sqrt(AC_cs2_sound)

    // Placeholders until determined properly
    k_force   = real3(AC_k_forcex,   AC_k_forcey,   AC_k_forcez  )
    ff_re     = real3(AC_ff_hel_rex, AC_ff_hel_rey, AC_ff_hel_rez)
    ff_im     = real3(AC_ff_hel_imx, AC_ff_hel_imy, AC_ff_hel_imz)

    // Determine that forcing funtion type at this point.
    force = helical_forcing(k_force, xx, ff_re, ff_im, AC_forcing_phase)

    // Scaling N = magnitude*cs*sqrt(k*cs/dt)  * dt
    NN = cs * AC_forcing_magnitude * sqrt(AC_kaver * cs)
    // sqrt(dt) because 1/sqrt(dt)*dt = sqrt(dt)
    force.x = sqrt(AC_dt) * NN * force.x
    force.y = sqrt(AC_dt) * NN * force.y
    force.z = sqrt(AC_dt) * NN * force.z

    return force
}
#endif // LFORCING


continuity() {
    return -dot(vecvalue(UU), gradient(VTXBUF_LNRHO)) - divergence(UU)
#if LUPWD
           + dot(vecvalue_abs(UU), gradient6_upwd(VTXBUF_LNRHO))
#endif

}

momentum() {
    S = stress_tensor(UU)
    cs2_sound = AC_cs_sound * AC_cs_sound
#if LENTROPY 
    cs2 = cs2_sound * exp(AC_gamma * value(VTXBUF_ENTROPY) / AC_cp_sound + (AC_gamma - 1.) * (value(VTXBUF_LNRHO) - AC_lnrho0))
#else
    cs2 = cs2_sound
#endif

#if LMAGNETIC
    j = (1. / AC_mu0) * (gradient_of_divergence(AA) - veclaplace(AA))
    B = curl(AA)
    inv_rho = 1. / exp(value(VTXBUF_LNRHO))
#endif

    mom = - gradients(UU) * vecvalue(UU)
#if LUPWD
          + gradients_upwd(UU) * vecvalue_abs(UU)
#endif
#if LENTROPY 
          - cs2 * ((1. / AC_cp_sound) * gradient(VTXBUF_ENTROPY) + gradient(VTXBUF_LNRHO))
#else
          - cs2 * gradient(VTXBUF_LNRHO)
#endif
#if LMAGNETIC
          + inv_rho * cross(j, B)
#endif
          + AC_nu_visc * (veclaplace(UU) + (1. / 3.) * gradient_of_divergence(UU)
                       + 2. * S * gradient(VTXBUF_LNRHO))
          + AC_zeta * gradient_of_divergence(UU)
#if LSHOCK
          + AC_nu_shock * (value(VTXBUF_SHOCK) * (divergence(UU) * gradient(VTXBUF_LNRHO) +
                                           gradient_of_divergence(UU)) +
                           divergence(UU) * gradient(VTXBUF_SHOCK))
#endif

    return mom
}

#if LENTROPY 
lnT() {
    return AC_lnT0
         + AC_gamma * value(VTXBUF_ENTROPY) / AC_cp_sound
         + (AC_gamma - 1.) * (value(VTXBUF_LNRHO) - AC_lnrho0)
}

heat_conduction() {
    inv_AC_cp_sound = 1. / AC_cp_sound
    grad_ln_chi = -gradient(VTXBUF_LNRHO)

    first_term = AC_gamma * inv_AC_cp_sound * laplace(VTXBUF_ENTROPY) + (AC_gamma - 1.) * laplace(VTXBUF_LNRHO)
    second_term = AC_gamma * inv_AC_cp_sound * gradient(VTXBUF_ENTROPY) + (AC_gamma - 1.) * gradient(VTXBUF_LNRHO)
    third_term = AC_gamma * (inv_AC_cp_sound * gradient(VTXBUF_ENTROPY) + gradient(VTXBUF_LNRHO)) + grad_ln_chi

    //chi = AC_THERMAL_CONDUCTIVITY / (exp(value(VTXBUF_LNRHO)) * AC_cp_sound)
    chi = AC_K_heatcond / (exp(value(VTXBUF_LNRHO)) * AC_cp_sound)

    return AC_cp_sound * chi * (first_term + dot(second_term, third_term))
}

entropy() {
    S = stress_tensor(UU)
    inv_pT = 1. / (exp(value(VTXBUF_LNRHO)) * exp(lnT()))
#if LMAGNETIC
    j = (1. / AC_mu0) * (gradient_of_divergence(AA) - veclaplace(AA))
#else
    j = real3(0,0,0)
#endif
#if LSHOCK
    entshock = AC_nu_shock * value(VTXBUF_SHOCK) * (divergence(UU) * divergence(UU))  
#else
    entshock = 0.0
#endif
    RHS = (0) - (0) + AC_eta * AC_mu0 * dot(j, j) +
                       2. * exp(value(VTXBUF_LNRHO)) * AC_nu_visc * contract(S) +
                       entshock +
                       AC_zeta * exp(value(VTXBUF_LNRHO)) * divergence(UU) * divergence(UU)

    return -dot(vecvalue(UU), gradient(VTXBUF_ENTROPY)) + inv_pT * RHS + heat_conduction()
}
#endif

#if LSHOCK
// Get divergence of velocity for the first pass in shock viscosity.
divu_shock()
{
    // Set discard the values which do not contain negative divergence.
    divu = divergence(UU)
    if divu < 0.0 {
        return -divu
    }
    else {
        return 0.0
    }
}

// Calculate local maximum from divergences.
max5_shock()
{
    return max5(VTXBUF_SHOCK)
}

Kernel shock_1_divu()
{
    write(VTXBUF_SHOCK, divu_shock())
}

Kernel shock_2_max()
{
    write(VTXBUF_SHOCK, max5_shock())
}

Kernel shock_3_smooth()
{
    dsmin = DSX //Here assuming that all spacing are equal
    cshock = 1.0  

    s_shock = smooth_kernel(VTXBUF_SHOCK)
    out_shock = cshock*dsmin*dsmin*s_shock //Add scaling constant cshock

    write(VTXBUF_SHOCK, out_shock)
}

#endif


rk3(s0, s1, roc) {
    /*
    real alpha = 0., -5./9., -153. / 128.
    real beta = 1. / 3., 15./ 16., 8. / 15.

    // This conditional has abysmal performance on AMD for some reason, better performance on NVIDIA than the workaround below
    if AC_step_number > 0 {
        return s1 + beta[AC_step_number] * ((alpha[AC_step_number] / beta[AC_step_number - 1]) * (s1 - s0) + roc * AC_dt)
    } else {
        return s1 + beta[AC_step_number] * roc * AC_dt
    }
    */
    // Workaround
    real alpha = 0., -5./9., -153. / 128.
    real beta  = 1., 1. / 3., 15./ 16., 8. / 15.

    /*
    // Commented out, BUG that NDEBUG is not enabled during code generation s.t. the following conditional
    // is evaluated, this destroys the performance
    #ifndef NDEBUG
    if (AC_step_number >= len(alpha) || AC_step_number + 1 >= len(beta)) {
        print("AC_step_number invalid: '%d'\n", AC_step_number)
    }
    #endif
    */

    return s1 + beta[AC_step_number + 1] * ((alpha[AC_step_number] / beta[AC_step_number]) * (s1 - s0) + roc * AC_dt)
    /*
    if AC_step_number == 0 {
        return s1 + (1. / 3.) * roc * AC_dt
    } else if AC_step_number == 1 {
        return s1 + (15./16.) * (((-5./9.) / (1./3.)) * (s1 - s0) + roc * AC_dt)
    } else {
        return s1 + (8./15.) * (((-153./128.) / (15./16.)) * (s1 - s0) + roc * AC_dt)
    }
    */
}


Kernel singlepass_solve() {
    write(VTXBUF_LNRHO, rk3(previous(VTXBUF_LNRHO), value(VTXBUF_LNRHO), continuity()))

#if LENTROPY 
    write(VTXBUF_ENTROPY, rk3(previous(VTXBUF_ENTROPY), value(VTXBUF_ENTROPY), entropy()))
#endif 

    mom = momentum()

#if LFORCING
    //Add the forcing component to the velocity field at the last integration step.
    //Important to init to zero here: ohterwise will produce numerical garbage! 
    forcing_step = real3(0.0, 0.0, 0.0)
    if AC_step_number == 2 {
        if AC_current_time > AC_switch_forcing {
            forcing_step = forcing()
        }
    }
    write(VTXBUF_UUX, rk3(previous(VTXBUF_UUX), value(VTXBUF_UUX), mom.x) + forcing_step.x)
    write(VTXBUF_UUY, rk3(previous(VTXBUF_UUY), value(VTXBUF_UUY), mom.y) + forcing_step.y)
    write(VTXBUF_UUZ, rk3(previous(VTXBUF_UUZ), value(VTXBUF_UUZ), mom.z) + forcing_step.z)
#else 
    write(VTXBUF_UUX, rk3(previous(VTXBUF_UUX), value(VTXBUF_UUX), mom.x))
    write(VTXBUF_UUY, rk3(previous(VTXBUF_UUY), value(VTXBUF_UUY), mom.y))
    write(VTXBUF_UUZ, rk3(previous(VTXBUF_UUZ), value(VTXBUF_UUZ), mom.z))
#endif

#if LMAGNETIC
    ind = induction()
    write(VTXBUF_AX, rk3(previous(VTXBUF_AX), value(VTXBUF_AX), ind.x))
    write(VTXBUF_AY, rk3(previous(VTXBUF_AY), value(VTXBUF_AY), ind.y))
    write(VTXBUF_AZ, rk3(previous(VTXBUF_AZ), value(VTXBUF_AZ), ind.z))
#endif

#if LBFIELD
    if AC_step_number == 2 {
        bfield = curl(AA) 
        write(BFIELDX, bfield.x)
        write(BFIELDY, bfield.y)
        write(BFIELDZ, bfield.z)
    }
#endif

#if LSHOCK
    // Required for buffers to match correctly. 
    write(VTXBUF_SHOCK, value(VTXBUF_SHOCK))
#endif

}

rk3_intermediate(w, roc) {
    real alpha = 0., -5./9., -153. / 128.
    return alpha[AC_step_number] * w + roc * AC_dt

    /*
    #ifndef NDEBUG
    if (AC_step_number >= len(alpha)) {
        print("AC_step_number invalid: '%d'\n", AC_step_number)
    }
    #endif

    // return alpha[AC_step_number] * w + roc * AC_dt

    // TODO NOTE abysmal performance on AMD, needs a workaround like in rk3() 
    if AC_step_number > 0 {
        return alpha[AC_step_number] * w + roc * AC_dt
    } else {
        return roc * AC_dt
    }
    */
}

rk3_final(f, w) {
    real beta = 1. / 3., 15./ 16., 8. / 15.

    /*
    #ifndef NDEBUG
    if (AC_step_number >= len(beta)) {
        print("AC_step_number invalid: '%d'\n", AC_step_number)
    }
    #endif
    */

    return f + beta[AC_step_number] * w
}


Kernel twopass_solve_intermediate() {
    write(VTXBUF_LNRHO, rk3_intermediate(previous(VTXBUF_LNRHO), continuity()))

    #if LENTROPY 
    write(VTXBUF_ENTROPY, rk3_intermediate(previous(VTXBUF_ENTROPY), entropy()))
    #endif

    mom = momentum()
    write(VTXBUF_UUX, rk3_intermediate(previous(VTXBUF_UUX), mom.x))
    write(VTXBUF_UUY, rk3_intermediate(previous(VTXBUF_UUY), mom.y))
    write(VTXBUF_UUZ, rk3_intermediate(previous(VTXBUF_UUZ), mom.z))

    #if LMAGNETIC
    ind = induction()
    write(VTXBUF_AX, rk3_intermediate(previous(VTXBUF_AX), ind.x))
    write(VTXBUF_AY, rk3_intermediate(previous(VTXBUF_AY), ind.y))
    write(VTXBUF_AZ, rk3_intermediate(previous(VTXBUF_AZ), ind.z))
    #endif

    #if LBFIELD
    if AC_step_number == 2 {
        bfield = curl(AA) 
        write(BFIELDX, bfield.x)
        write(BFIELDY, bfield.y)
        write(BFIELDZ, bfield.z)
    }
    #endif

    // %JP: TODO NOTE IMPORTANT (MV please check)
    // LSHOCK not yet adapted to two-pass integration.
    // Unclear how should be written to memory to ensure proper swapping!
    // 1) In the single-pass approach need to write the shock field to `out`
    // because buffers are swapped afterwards
    // 2) In the two-pass approach this may not be necessary: if the shock
    // field is in `in` buffer and we swap twice, the correct shock values are
    // still in the `in` buffer
    //
    // Is this correct?
    //
}

Kernel twopass_solve_final() {
    write(VTXBUF_LNRHO, rk3_final(previous(VTXBUF_LNRHO), value(VTXBUF_LNRHO)))

    #if LENTROPY 
    write(VTXBUF_ENTROPY, rk3_final(previous(VTXBUF_ENTROPY), value(VTXBUF_ENTROPY)))
    #endif

    forcing_step = real3(0.0, 0.0, 0.0)
    #if LFORCING
    if AC_step_number == 2 {
        if AC_current_time > AC_switch_forcing {
            forcing_step = forcing()
        }
    }
    #endif
    write(VTXBUF_UUX, rk3_final(previous(VTXBUF_UUX), value(VTXBUF_UUX)) + forcing_step.x)
    write(VTXBUF_UUY, rk3_final(previous(VTXBUF_UUY), value(VTXBUF_UUY)) + forcing_step.y)
    write(VTXBUF_UUZ, rk3_final(previous(VTXBUF_UUZ), value(VTXBUF_UUZ)) + forcing_step.z)

    #if LMAGNETIC
    write(VTXBUF_AX, rk3_final(previous(VTXBUF_AX), value(VTXBUF_AX)))
    write(VTXBUF_AY, rk3_final(previous(VTXBUF_AY), value(VTXBUF_AY)))
    write(VTXBUF_AZ, rk3_final(previous(VTXBUF_AZ), value(VTXBUF_AZ)))
    #endif

    #if LBFIELD
    if AC_step_number == 2 {
        write(BFIELDX, value(BFIELDX))
        write(BFIELDY, value(BFIELDY))
        write(BFIELDZ, value(BFIELDZ))
    }
    #endif
}

// Scale all fields by a scaling factor
// TODO ensure AC_scaling_factor is set!
real AC_scaling_factor
Kernel scale() {
    for field in 0:NUM_FIELDS {
        write(Field(field), value(field))
    }

#if LMAGNETIC
    write(VTXBUF_AX, AC_scaling_factor * value(VTXBUF_AX))
    write(VTXBUF_AY, AC_scaling_factor * value(VTXBUF_AY))
    write(VTXBUF_AZ, AC_scaling_factor * value(VTXBUF_AZ))
#else
    if vertexIdx.x == AC_mx/2 && vertexIdx.y == AC_my/2 && vertexIdx.z == AC_mz/2 {
        print("WARNING: scale kernel called but LMAGNETIC was not enabled. Scale will have no effect.")
    }
#endif
}

Kernel reset() {
    for field in 0:NUM_FIELDS {
        write(Field(field), 0.0)
    }
}

/*
xorshift(state) {
    x = 1 + uint64_t(state)
    x ^= x << 12
    x ^= x >> 25
    x ^= x << 27
    return x * 0x2545F4914F6CDD1D
}

Kernel randomize() {
    // N.B. scale: result in [-AC_rng_scale, AC_rng_scale] range
    AC_rng_scale = 1e-5

    for field in 0:NUM_FIELDS {
        i = uint64_t(globalVertexIdx.x) + uint64_t(globalVertexIdx.y) * uint64_t(globalGridN.x) + uint64_t(globalVertexIdx.z) * uint64_t(globalGridN.x) * uint64_t(globalGridN.y) + uint64_t(field) * uint64_t(globalGridN.x) * uint64_t(globalGridN.y) * uint64_t(globalGridN.z)
        //i += i*i + i*i*i + i*i*i*i + i*i*i*i*i // Add some non-linearity
        
        // Generate some entropy
        for step in 0:10 {
            i = xorshift(i)
        }
        for step in 0:(i % 40) {
            i = xorshift(i)
        }

        // Simple linear congruential RNG
        //write(Field(field), 2.0 * ((75 * i + 74 + 3257*i*i + 6067*i*i*i + 7919*i*i*i*i)%999331) / 999331 - 1.0)
        
        // xorshift*
        //seed = uint64_t(globalGridN.x) * uint64_t(globalGridN.y) * uint64_t(globalGridN.z) * uint64_t(NUM_FIELDS)
        r = 2.0 * xorshift(i) / UINT64_MAX - 1.0
        write(Field(field), AC_rng_scale * r)
    }
}
*/

Kernel randomize() {
    
    // N.B. scale: result in (-AC_rng_scale, AC_rng_scale] range
    AC_rng_scale = 1e-5

    for field in 0:NUM_FIELDS {
        r = 2.0 * rand_uniform() - 1.0
        write(Field(field), AC_rng_scale * r)
    }
}

Kernel haatouken() {
    // Parameters
    // Shock punch the air in front of you.
    // AC_init_ampl_uu
    // AC_init_shell_radius
    // AC_init_shell_width

    uu = vecvalue(UU)

    xx = grid_position()

    centre = grid_centre()

    xx.x = xx.x - centre.x
    xx.y = xx.y - centre.y
    xx.z = xx.z - centre.z

    rr = sqrt(xx.x*xx.x +  xx.y*xx.y + xx.z*xx.z)

    theta = real(atan2(xx.z, xx.x))
    phi   = real(atan2(xx.y, xx.x))

    uu_radial = 0.0

    if rr > 2.0*AC_dsx { 
        uu_radial = AC_init_ampl_uu * 
                    exp(-pow((rr - AC_init_shell_radius), 2.0) / (2.0 * pow(AC_init_shell_width, 2.0))) *
                    exp(-pow(theta , 2.0) / (2.0 * pow(R_PI / 8.0, 2.0))) *
                    exp(-pow(phi, 2.0) / (2.0 * pow(R_PI / 8.0, 2.0)))
    } 

    //if (rr > 0.8 && rr < 1.2) {
    //    if (phi < R_PI/8.0 && phi > -R_PI/8.0) {
    //        if (theta < R_PI/8 && theta > -R_PI/8) {
    //            uu_radial = AC_init_ampl_uu 
    //        }
    //    }
    //} 

    uu.x = uu.x + uu_radial * cos(theta) * cos(phi)
    uu.y = uu.y + uu_radial * cos(theta) * sin(phi)
    uu.z = uu.z + uu_radial * cos(theta) 

    write(VTXBUF_UUX, uu.x)
    write(VTXBUF_UUY, uu.y)
    write(VTXBUF_UUZ, uu.z)


}

beltrami(ampl, ex, ey, ez, kx, ky, kz, phase) {
    // Based on Pencil Code beltrami_general() 
    // https://github.com/pencil-code/pencil-code/blob/43f0b658a905bbeb641daa0ccf88720a20f0c562/src/initcond.f90#L1818C16-L1818C32

    kxe_x = ky*ez - kz*ey
    kxe_y = kz*ex - kx*ez
    kxe_z = kx*ey - ky*ex

    kxkxe_x = ky*kxe_z - kz*kxe_y
    kxkxe_y = kz*kxe_x - kx*kxe_z
    kxkxe_z = kx*kxe_y - ky*kxe_x

    kk=sqrt(kx*kx+ky*ky+kz*kz)

    xx = grid_position()

    cfunc = abs(ampl) * cos(kx*xx.x + ky*xx.y + kz*xx.z + phase)
    sfunc =     ampl  * sin(kx*xx.x + ky*xx.y + kz*xx.z + phase)

    field = real3(kxkxe_x*cfunc + kk*kxe_x*sfunc,   
                  kxkxe_y*cfunc + kk*kxe_y*sfunc,   
                  kxkxe_z*cfunc + kk*kxe_z*sfunc)

    return field
}

Kernel beltrami_initcond() {
    //uu = beltrami(1e-5, 0.0, 0.0, 1.0, 3.0, 2.0, 3.0, 0.0)
    aa = beltrami(1e-5, 0.0, 0.0, 1.0, 6.0, 4.0, 6.0, 0.0)

    //write(VTXBUF_UUX, uu.x)
    //write(VTXBUF_UUY, uu.y)
    //write(VTXBUF_UUZ, uu.z)
    write(VTXBUF_AX, aa.x)
    write(VTXBUF_AY, aa.y)
    write(VTXBUF_AZ, aa.z)
}

radial_vector_field(ampl) {
    xx = grid_position()
    centre = grid_centre()

    xx.x = xx.x - centre.x
    xx.y = xx.y - centre.y
    xx.z = xx.z - centre.z

    rr = sqrt(xx.x*xx.x + xx.y*xx.y + xx.z*xx.z)
    rr_xy = sqrt(xx.x*xx.x + xx.y*xx.y )

    field = real3(0.0,
                  0.0,
                  0.0)

    if rr >= 2.0*DSX {

        theta = real(atan2(rr_xy, xx.z))

        phi   = real(atan2(xx.y, xx.x))

        field = real3(ampl * cos(theta)* cos(phi),  
                      ampl * cos(theta)* sin(phi),  
                      ampl * cos(theta))
    }

    return field
}

Kernel radial_vec_initcond() {
    uu = radial_vector_field(-1e-5) 

    write(VTXBUF_UUX, uu.x)
    write(VTXBUF_UUY, uu.y)
    write(VTXBUF_UUZ, uu.z)
}

Kernel constant() {
    
    AC_value = 1.0

    for field in 0:NUM_FIELDS {
        write(Field(field), AC_value)
    }
}
