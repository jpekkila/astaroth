

/*
 * =============================================================================
 * "Compile-time" params
 * =============================================================================
 */
AC_nx = 256 
AC_ny = 256 
AC_nz = 256 

AC_dsx = 0.012271846303085129837744700715935558141395192
AC_dsy = 0.012271846303085129837744700715935558141395192
AC_dsz = 0.012271846303085129837744700715935558141395192

// 0 = periodic bc, 1 = symmetric bc, 2 = antisymmetric bc
AC_bc_type_top_x = BOUNDCOND_PERIODIC
AC_bc_type_top_y = BOUNDCOND_PERIODIC
AC_bc_type_top_z = BOUNDCOND_PERIODIC
AC_bc_type_bot_x = BOUNDCOND_PERIODIC
AC_bc_type_bot_y = BOUNDCOND_PERIODIC
AC_bc_type_bot_z = BOUNDCOND_PERIODIC


/*
 * =============================================================================
 * Run-time params
 * =============================================================================
 */
AC_max_steps  = 300001
AC_save_steps = 10
AC_slice_steps = 1000000
AC_bin_steps  = 100000 
AC_bin_save_t = 1.0
AC_slice_save_t = 0.03

// Set AC_num_snapshots < 0 if you want to decide snapshot number by time
// interval
AC_num_snapshots = -1 

AC_forcing_period_steps = 1
AC_forcing_period_t = 0.0

// Set to 0 if you want to run the simulation from the beginning, or just a new
// simulation. If continuing from a saved step, specify the step number here.
AC_start_step = 0

// Maximum time in code units. If negative, there is no time limit
AC_max_time = 6.4

// Hydro
AC_cdt = 0.4
AC_cdtv = 0.3
AC_cdts = 1.0
//AC_nu_visc  = 1e-3
AC_nu_visc  = 1e-3
AC_cs_sound = 1.0
AC_zeta = 0.00

// Magnetic
//AC_eta = 1e-3
AC_eta = 1e-3 
AC_mu0 = 1.0
AC_chi = 0.0001

// Forcing
AC_relhel = 1.0
//AC_forcing_magnitude = 0.08
AC_forcing_magnitude = 0.8
AC_kmin              = 4.5
AC_kmax              = 5.5
// Switches forcing off and accretion on
AC_switch_accretion  = 0  
AC_switch_forcing    = 0.0

// Entropy
AC_cp_sound = 1.0
AC_gamma = 0.5
AC_lnT0 = 1.2
AC_lnrho0 = 0.0
AC_K_heatcond = 1e-3

// Sink Particle
AC_sink_pos_x = 3.14
AC_sink_pos_y = 3.14
AC_sink_pos_z = 3.14
AC_M_sink_Msun = 1.0
AC_soft = 0.12

// Accretion Parameters
// profile_range is multiple of dsx
AC_accretion_range = 2.0

// Physical properties of the domain
AC_unit_velocity = 1.0
AC_unit_density = 1.0
AC_unit_length = 1.0

// Shock viscosity
AC_nu_shock = 1.0

// Coordinate based reduction test. 
AC_center_x = 1.57 
AC_center_y = 1.57 
AC_center_z = 1.57 
AC_window_radius = 0.5


/*
 * =============================================================================
 * Initial conditions
 * =============================================================================
 */
AC_ampl_lnrho = 0.0
AC_ampl_uu = 0.0

AC_init_ampl_uu      = 1.8
AC_init_shell_radius = 0.6 
AC_init_shell_width  = 0.2

AC_init_type = INIT_TYPE_RANDOM 

